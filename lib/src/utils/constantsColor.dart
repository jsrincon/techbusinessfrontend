import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConstantsColors {
  static const Color primaryColor = Color(0xff50459B);

  static const Color secondaryColor = Color(0xff5d549a);

  static const Color disableColor = Color(0xaaefefef);

  static const Color error = Color(0xf74e0009);

  static const Color success = Color(0xf74ba826);

  static const Color blueDark = Color(0xaa00314e);
  static const Color black = Colors.black;
  static const Color darkGray = Color(0xff555555);
  static const Color moderateGray = Color(0xff999999);
  static const Color normalGray = Color(0xffc0c0c0);
  static const Color lightGray = Color(0xffcccccc);
  static const Color veryLightGray = Color(0xffefefef);
  static const Color white = Colors.white;

}
