import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';

void confirmDialog(BuildContext context, String title, String message) {}

void showAlert(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text('error')),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.error,
                color: ConstantsColors.error,
              ),
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ConstantsColors.black,
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text(Constants.btnCancel),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
}

void showDialogSuccess(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text('Success')),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check,
                color: ConstantsColors.success,
              ),
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ConstantsColors.black,
                  ),
                ),
              )
            ],
          ),
        );
      });
}