import 'package:flutter/material.dart';

class ListItemDescription extends StatelessWidget {
  const ListItemDescription({
    Key key,
    this.title,
    this.description,
    this.status,
  }) : super(key: key);

  final String title;
  final String description;
  final bool status;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18.0,
            ),
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          Text(
            description,
            style: const TextStyle(fontSize: 16.0),
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 1.0)),
          Text(
            '$status',
            style: const TextStyle(fontSize: 12.0),
          ),
        ],
      ),
    );
  }
}
