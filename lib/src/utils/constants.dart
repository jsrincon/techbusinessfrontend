class Constants {
  static const String urlAuthority =
      'ec2-18-221-248-190.us-east-2.compute.amazonaws.com:8089';
  static const String pathBase = '/api/tech_business';
  static const String contentTypeHeader = 'application/json; charset=utf-8';
  static const String authorizationHeader = 'Bearer ';

  //Url Backend authentication
  static const String urlLogin = pathBase + '/auth/login';
  static const String urlRegisterUser = pathBase + '/auth/register';

  //Url Backend employee
  static const String urlEmployee = pathBase + '/employee';

  //Url Backend process
  static const String urlProcess = pathBase + '/process';

  //Url Backend services
  static const String urlService = pathBase + '/services';

  static const String urlEquipment = pathBase + '/equipment';

  //App
  static const String appName = 'Tech Business';
  static const String logout = 'Cerrar Sesión';

  //appBarTitle
  static const String appBarMenu = 'Tech Business menu';
  static const String appBarRegisterWorker = 'Registrar Trabajador';
  static const String appBarProcessList = 'Lista de procesos';
  static const String appBarregisterEmployee = 'Registrar Trabajador';

  //Route
  static const String homeRoute = '/';
  static const String authUiRoute = '/auth_login_ui';
  static const String administratorUiRoute = '/administrator_ui';
  static const String employeeUiRoute = '/employee_ui';
  static const String processUiRoute = '/process_ui';
  static const String serviceUiRoute = '/service_ui';
  static const String serviceDetailUiRoute = '/detail_service_ui';
  static const String equipmentUiRoute = '/equipment_ui';
  static const String clientUiRoute = '/client_ui';
  static const String configUiRoute = '/config_ui';
  static const String employeeSeachUiRoute = '/employee_seach_ui';
  static const String employeeListUiRoute = '/employee_list_ui';

  //MenuTitle
  static const String menuTitleEmployee = 'Trabajadores';
  static const String menuTitleProcess = 'Procesos';
  static const String menuTitleService = 'Servicios';
  static const String menuTitleClient = 'Clientes';
  static const String menuTitleConfig = 'Configuración';
  static const String menuTitleEquipment = 'Equipos';

  //Message
  static const String messageSuccessfulRegister = '¡Registro exitoso!';
  static const String messageLogoutConfirm =
      'Esta seguro de finalizar la sesión.';
  static const String messageAlertDialog2 =
      'Esta seguro de finalizar la acción.';
  static const String messageAlertDialog3 = 'Formule el proceso.';
  static const String loginSuccess = 'inicio de sesion exitoso';
  static const String insertSuccess = 'registro exitoso';
  static const String updateSuccess = 'actualización exitoso';
  static const String deleteSuccess = 'Se eliminó correctamente';
  static const String loginTitle = 'Login';
  static const String registerTitle = 'Register';

  //Button
  static const String btnAccept = 'Aceptar';
  static const String btnCancel = 'Cancelar';
  static const String btnRegister = 'Registrar';
  static const String btnCreates = 'Crear';
  static const String btnUpdates = 'Actualizar';
  static const String btnExit = 'Cerrar';
  static const String btnSignIn = 'Ingresar';

  //Label forms
  static const String labelName = 'Nombre';
  static const String labelLastName = 'Apellido';
  static const String labelEmail = 'Correo Electronico';
  static const String labelCCorNit = 'Cedula o Nit';
  static const String labelCellPhone = 'Celular';
  static const String labelAddresses = 'Dirección';
  static const String labelDescription = 'Descripción';
  static const String labelProcessState = 'Estado del proceso';

  //label action
  static const String labelCreates = 'Crear';
  static const String labelAdd = 'Agregar';
  static const String labelUpdates = 'Actualizar';

  //label module
  static const String labelEmployee = 'Trabajadores';
  static const String labelProcess = 'Procesos';
  static const String labelChecklistItem = 'Item check';
  static const String labelService = 'Servicios';
  static const String labelClient = 'Clientes';
  static const String labelConfig = 'Configuración';
  static const String labelEquipment = 'Equipos';

  //hint
  static const String someText = 'Por favor ingrese un texto';

  //validation text fields
  static const String structure = 'La estructura no coincide con la solicitada';

  static const String validateName = 'El nombre es necesario';
  static const String validateLastName = 'El apellido es necesario';
  static const String fieldStructureUpperAndLower =
      'El campo solo puede contener a-z y A-Z';
  static const String patternUpperAndLower = r'(^[a-zA-Z ]*$)';

  static const String validateEmail = 'El correo es necesario';
  static const String emailStructure = 'Correo invalido';
  static const String patternEmail =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static const String validateMobile = 'El telefono es necesario';
  static const String mobileStructure = 'Debe tener 10 digitos';
  static const String patterMobile = r'(^[3][0-9]*$)';

  static const String identificationStructure = 'Debe tener 10 digitos';
  static const String validateIdentification =
      'La documentacion o Nit es necesario';
  static const String patterIdentification = r'(^[3][0-9]*$)';

  static const String confirmDelete = 'Are you sure you want to delete?';

  static const String actionDelete = 'eliminar';
  static const String actionSave = 'guardar';
  static const String actionEdit = 'editar';
  static const String actionShow = 'ver más';
  static const String actionChecklist = 'lista de chequeo';

  static const List<String> actionsMenu = ['guardar','eliminar'];
}
