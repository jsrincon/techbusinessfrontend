import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class ServiceApiService {
  ApiResponse _apiResponse;
  Service _service;
  ErrorApiResponse _error;

  ServiceApiService();

  Future<ApiResponse> create(Service service, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(service.toJsonRegistry());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlService);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(res.body);
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _service = Service.fromJson(resBody);
      _apiResponse.payload = _service;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Service service, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(service.toJson());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlService);
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(res.body);
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _service = Service.fromJson(resBody);
      _apiResponse.payload = _service;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> delete(int id, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var pathVariable = '/' + id.toString();
    var uri =
        Uri.http(Constants.urlAuthority, Constants.urlService + pathVariable);
    var res = await http.delete(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
    });
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.payload = Constants.deleteSuccess;
    } else {
      var resBody = json.decode(res.body);
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getAll(String accessToken) async {
    List<Service> listService;
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlService);
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken,
    });
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listService = [];
    if (apiResponse.statusResponse == HttpStatus.ok) {
      resBody.forEach((process) {
        listService.add(Service.fromJson(process));
      });
      apiResponse.payload = listService;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
