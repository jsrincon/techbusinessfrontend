import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/model/user_model.dart';
import 'package:techbusinessfrontend/src/repository/auth_api_service.dart';
import 'package:techbusinessfrontend/src/repository/employee_api_service.dart';
import 'package:techbusinessfrontend/src/repository/equipment_api_service.dart';
import 'package:techbusinessfrontend/src/repository/process_api_service.dart';
import 'package:techbusinessfrontend/src/repository/service_api_service.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

class GeneralRepository with TokenHelper {
  final authApiService = AuthApiService();
  final employeeApiService = EmployeeApiService();
  final processApiService = ProcessApiService();
  final serviceApiService = ServiceApiService();
  final equipmentApiService = EquipmentApiService();

//Auth
  Future<ApiResponse> login(LoginRequest loginRequest) =>
      authApiService.login(loginRequest);

  Future<ApiResponse> signUp(User user) => authApiService.signup(user);

  Future<ApiResponse> register(Employee employee) =>
      employeeApiService.register(employee, accessToken);

  Future<ApiResponse> updateEmployee(Employee employee) =>
      employeeApiService.update(employee, accessToken);

  Future<ApiResponse> listEmployee() => employeeApiService.getAll(accessToken);

  //Process
  Future<ApiResponse> getAllProcess() => processApiService.getAll(accessToken);

  Future<ApiResponse> createProcess(Process process) =>
      processApiService.create(process, accessToken);

  Future<ApiResponse> updateProcess(Process process) =>
      processApiService.update(process, accessToken);

  //Service
  Future<ApiResponse> getAllService() => serviceApiService.getAll(accessToken);

  Future<ApiResponse> createService(Service service) =>
      serviceApiService.create(service, accessToken);

  Future<ApiResponse> updateService(Service service) =>
      serviceApiService.update(service, accessToken);

  Future<ApiResponse> deleteService(int id) =>
      serviceApiService.delete(id, accessToken);

  //Equipment

  Future<ApiResponse> getAllEquipment() =>
      equipmentApiService.getAll(accessToken);

  Future<ApiResponse> createEquipment(Equipment equipment) =>
      equipmentApiService.create(equipment, accessToken);

  Future<ApiResponse> updateEquipment(Equipment equipment) =>
      equipmentApiService.update(equipment, accessToken);
}
