import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class EmployeeBloc {
  final _repository = GeneralRepository();
  ApiResponse _apiResponse = ApiResponse();
  final _employeeController = StreamController.broadcast();
  final _employeeListController = StreamController<List<Employee>>.broadcast();
  List<Employee> _initialList;

  Stream<Employee> get register =>
      _employeeController.stream.asBroadcastStream();

  Stream<List<Employee>> get employeeList =>
      _employeeListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  EmployeeBloc();

  Future<ApiResponse> create(Employee employee) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.register(employee);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Employee employee) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateEmployee(employee);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.listEmployee();
    if (_apiResponse.statusResponse == 200) {
      _initialList = _apiResponse.payload;
      _apiResponse.message = Constants.insertSuccess;
      _employeeListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  void dispose() {
    _employeeController.close();
    _employeeListController.close();
  }
}
