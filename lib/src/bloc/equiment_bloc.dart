import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class EquipmentBLoc {
  final _repository = GeneralRepository();
  ApiResponse _apiResponse = ApiResponse();
  final _serviceListController = StreamController<List<Equipment>>.broadcast();
  final _serviceController = StreamController.broadcast();
  List<Equipment> _initialList;

  Stream<Equipment> get equipment => _serviceController.stream.asBroadcastStream();


  Stream<List<Equipment>> get equipmentList =>
      _serviceListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  EquipmentBLoc();

  Future initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllEquipment();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _serviceListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> create(Equipment equipment) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.createEquipment(equipment);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Equipment equipment) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateEquipment(equipment);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _serviceController.close();
    _serviceListController.close();
  }
}
