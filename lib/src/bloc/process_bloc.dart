import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class ProcessBloc {
  final _repository = GeneralRepository();
  ApiResponse _apiResponse = ApiResponse();
  final _processListController = StreamController<List<Process>>.broadcast();
  final _processController = StreamController.broadcast();
  List<Process> _initialList;

  Stream<Process> get process => _processController.stream.asBroadcastStream();

  Stream<List<Process>> get processList =>
      _processListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  ProcessBloc();

  Future initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllProcess();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _processListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> create(Process process) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.createProcess(process);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Process process) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateProcess(process);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _processController.close();
    _processListController.close();
  }
}
