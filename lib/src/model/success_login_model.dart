class SuccessLogin {
  String token;

  SuccessLogin({this.token});

  factory SuccessLogin.fromJson(Map<String, dynamic> parsedJson) {
    return SuccessLogin(token: parsedJson['token']);
  }

  Map<String, dynamic> toJson() => {
        'token': token,
      };
}
