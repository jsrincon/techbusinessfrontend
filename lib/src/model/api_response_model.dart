class ApiResponse {
  int statusResponse;
  Object payload;
  String message;

  ApiResponse({this.statusResponse, this.payload});
  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
        statusResponse: json['statusResponse'], payload: json['payload']);
  }

  Map<String, dynamic> toJson() =>
      {'statusResponse': statusResponse, 'payload': payload};
}
