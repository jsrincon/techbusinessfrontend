import 'type_user_model.dart';

class Employee {
  int id;
  String name;
  String lastName;
  String email;
  String address;
  String phone;
  String username;
  String password;
  bool status;
  String document;
  List<Role> roles;

  Employee(
      {this.id,
      this.name,
      this.lastName,
      this.email,
      this.address,
      this.phone,
      this.username,
      this.password,
      this.status,
      this.document,
      this.roles});

  factory Employee.fromJson(Map<String, dynamic> parsedJson) {
    var role = parsedJson['roles'] as List;

    var roleList =
        role != null ? role.map((i) => Role.fromJson(i)).toList() : null;

    return Employee(
        id: parsedJson['id'],
        name: parsedJson['name'],
        lastName: parsedJson['lastname'],
        email: parsedJson['email'],
        address: parsedJson['address'],
        phone: parsedJson['phone'],
        username: parsedJson['username'],
        password: parsedJson['password'],
        status: parsedJson['status'],
        document: parsedJson['document'],
        roles: roleList);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lastName': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'username': username,
        'password': password,
        'status': status,
        'document': document,
        'roles': roles != null ? roles.map((i) => i.toJson()).toList() : null
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastName': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'username': username,
        'password': password,
        'status': status,
        'document': document,
      };
}
