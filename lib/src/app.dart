import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/ui/administrator/administrator_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_list_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_seach_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/equipment/equipment_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/process_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/service/service_ui.dart';
import 'package:techbusinessfrontend/src/ui/authentication/auth_login_ui.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var token = TokenHelper();
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: ConstantsColors.primaryColor,
        accentColor: ConstantsColors.secondaryColor,
        iconTheme: IconThemeData(
          color: ConstantsColors.darkGray,
          size: 48
        ),
        buttonColor: ConstantsColors.primaryColor,
        fontFamily: 'CenturyGothic',
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
          headline2: TextStyle(fontSize: 26.0, fontWeight: FontWeight.normal),
          headline3: TextStyle(fontSize: 22.0, fontStyle: FontStyle.italic),
          bodyText1: TextStyle(fontSize: 20.0, fontFamily: 'Hind'),
          bodyText2: TextStyle(fontSize: 16.0, fontFamily: 'Hind'),
        ),
      ),
      debugShowCheckedModeBanner: false,
      title: Constants.appName,
      initialRoute: token.existToken() == false
          ? Constants.authUiRoute
          : Constants.administratorUiRoute,
      routes: {
        Constants.authUiRoute: (context) => AuthUi(),
        Constants.homeRoute: (context) => AdministratorUi(),
        Constants.administratorUiRoute: (context) => AdministratorUi(),
        Constants.serviceUiRoute: (context) => ServiceUI(),
        Constants.employeeUiRoute: (context) => EmployeeUi(),
        Constants.processUiRoute: (context) => ProcessUI(),
        Constants.employeeSeachUiRoute: (context) => EmployeeSeachUi(),
        Constants.employeeListUiRoute: (context) => EmployeeListUi(),
        Constants.equipmentUiRoute: (context) => EquipmentUi()
      },
    );
  }
}
