import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/auth_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/user_model.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class RegisterUi extends StatefulWidget {
  const RegisterUi({Key key}) : super(key: key);

  @override
  RegisterUiState createState() => RegisterUiState();
}

class RegisterUiState extends State<RegisterUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();

  RegisterUiState();

  AuthBloc registerBloc;
  User user = User();
  DateTime selectedDate = DateTime.now();

  @override
  void dispose() {
    super.dispose();
    registerBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    registerBloc = AuthBloc();
  }

  void _submitRegisterForm() {
    final formRegister = _formRegister.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      registerBloc.signUp(user).then((ApiResponse data) {
        if (data.statusResponse == 200) {
          Navigator.pushReplacementNamed(context, 'auth');
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.registerTitle),
          backgroundColor: Colors.indigo,
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Form(

                key: _formRegister,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          icon: Icon(Icons.person),
                          hintText: 'Username'),
                      onSaved: (String value) {
                        user.username = value;
                      },
                    ),
                    Divider(),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          icon: Icon(Icons.vpn_key),
                          hintText: 'Password'),
                      onSaved: (String value) {
                        user.password = value;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          _submitRegisterForm();
                        },
                        child: Text('Register'),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
