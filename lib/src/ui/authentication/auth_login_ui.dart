import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/auth_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';

import '../../navegator_techbusiness.dart';

class AuthUi extends StatefulWidget {
  const AuthUi({Key key}) : super(key: key);

  @override
  AuthUiState createState() => AuthUiState();
}

class AuthUiState extends State<AuthUi> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formLogin = GlobalKey<FormState>();

  AuthUiState();

  AuthBloc authBloc;
  LoginRequest loginRequestForm = LoginRequest();

  @override
  void dispose() {
    super.dispose();
    authBloc.dispose();
  }

  @override
  void initState() {
    super.initState();

    authBloc = AuthBloc();
  }

  void _submitLoginForm() {
    final formLogin = _formLogin.currentState;

    if (formLogin.validate()) {
      formLogin.save();
      authBloc.signIn(loginRequestForm).then((ApiResponse data) {
        if (data.statusResponse == 200) {
          TechBusinessNavigator.goToAdministratorUi(context);
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  Widget _form(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          key: _formLogin,
          child: Column(
            children: <Widget>[
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    icon: Icon(Icons.person), hintText: 'username'),
                onSaved: (String value) {
                  loginRequestForm.username = value;
                },
              ),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                obscureText: true,
                decoration: InputDecoration(
                    icon: Icon(Icons.no_encryption), hintText: 'password'),
                onSaved: (value) {
                  loginRequestForm.password = value;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 48.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: ConstantsColors.primaryColor,
                      onPrimary: ConstantsColors.primaryColor),
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
                    child: Text(
                      Constants.btnSignIn,
                      style: TextStyle(color: ConstantsColors.disableColor),
                    ),
                  ),
                  onPressed: () {
                    _submitLoginForm();
                  },
                ),
              ),
            ],
          ),
        ));
  }

  Widget _loginForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Column(
      children: [
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          width: size.width * 0.85,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: ConstantsColors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Text('Login',
                  style: TextStyle(color: ConstantsColors.secondaryColor)),
              SizedBox(height: 60.0),
              _form(context),
              SizedBox(height: 100.0)
            ],
          ),
        ),
      ],
    ));
  }

  Widget _createBackground(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final background = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Theme.of(context).accentColor,
        ConstantsColors.secondaryColor,
      ])),
    );

    return Stack(
      children: <Widget>[
        background,
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.home_repair_service,
                color: ConstantsColors.moderateGray,
                size: 100,
              ),
              SizedBox(height: 10.0, width: double.infinity),
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        fit: StackFit.expand,
        children: [_createBackground(context), _loginForm(context)],
      ),
      backgroundColor: Colors.white60,
    );
  }
}
