import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/process_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';

class ProcessUI extends StatefulWidget {
  const ProcessUI({Key key}) : super(key: key);

  @override
  ProcessUiState createState() => ProcessUiState();
}

class ProcessUiState extends State<ProcessUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formProcess = GlobalKey<FormState>();

  ProcessUiState();

  List<Process> lstProcess = [];
  Process process = Process();
  ProcessBloc processBloc;

  @override
  void dispose() {
    super.dispose();
    processBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    processBloc = ProcessBloc();
  }

  void _submitChangeStatus(newStatus, Process process) {
    process.status = newStatus;
    processBloc.update(process).then((ApiResponse data) {
      if (data.statusResponse == 200) {
        processBloc.initializeData();
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  void _submitRegisterForm() {
    final formRegister = _formProcess.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      process.status = true;
      processBloc.create(process).then((ApiResponse data) {
        if (data.statusResponse == 201) {
          processBloc.initializeData();
          Navigator.of(context).pop();
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _submitUpdateForm() {
    final formRegister = _formProcess.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      processBloc.update(process).then((ApiResponse data) {
        if (data.statusResponse == 200) {
          processBloc.initializeData();
          Navigator.of(context).pop();
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    processBloc.initializeData();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelProcess),
        backgroundColor: ConstantsColors.primaryColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacementNamed(
                context, Constants.administratorUiRoute);
          },
          child: Icon(
            Icons.arrow_back,
            size: 24,
          ),
        ),
      ),
      body: _buildStream(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (_) => _buildPopupDialog(context, null, true),
          );
        },
        child: Icon(
          Icons.add,
          size: 24,
        ),
        backgroundColor: ConstantsColors.secondaryColor,
      ),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<List<Process>>(
        stream: processBloc.processList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Process> data) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverPadding(
                padding: const EdgeInsets.all(10.0),
                sliver: SliverList(
                    delegate: SliverChildListDelegate(<Widget>[
                  Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      margin: EdgeInsets.all(5),
                      elevation: 5,
                      color: ConstantsColors.veryLightGray,
                      child: Column(children: [
                        ListTile(
                          title: Text(data[index].name),
                          subtitle: Text(data[index].description),
                          trailing: Wrap(
                            children: [
                              IconButton(
                                icon: Icon(Icons.edit),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (_) => _buildPopupDialog(
                                        context, data[index], false),
                                  );
                                },
                              ),
                              Switch(
                                value: data[index].status,
                                onChanged: (bool newValue) {
                                  _submitChangeStatus(newValue, data[index]);
                                },
                              ),
                            ],
                          ),
                          isThreeLine: true,
                        )
                      ]))
                ])),
              )
            ],
          );
        });
  }

  Widget _buildPopupDialog(
      BuildContext context, Process processLoad, bool isNew) {
    isNew ? process = Process() : process = processLoad;
    var title = isNew
        ? Constants.labelCreates + ' ' + Constants.labelProcess.toLowerCase()
        : Constants.labelUpdates + ' ' + Constants.labelProcess.toLowerCase();
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Form(
            key: _formProcess,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: isNew ? '' : process.name,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelName),
                  onSaved: (String value) {
                    process.name = value;
                  },
                ),
                TextFormField(
                  initialValue: isNew ? '' : process.description,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  maxLines: 2,
                  maxLength: 150,
                  decoration:
                      InputDecoration(hintText: Constants.labelDescription),
                  onSaved: (String value) {
                    process.description = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                Constants.btnCancel,
                style: TextStyle(color: ConstantsColors.primaryColor),
              ),
            ),
            isNew
                ? ElevatedButton(
                    onPressed: () {
                      _submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnCreates))
                : ElevatedButton(
                    onPressed: () {
                      _submitUpdateForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnUpdates)),
          ],
        )
      ],
    );
  }
}
