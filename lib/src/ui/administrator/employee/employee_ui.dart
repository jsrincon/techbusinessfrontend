import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/employee_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';

class EmployeeUi extends StatefulWidget {
  const EmployeeUi({Key key}) : super(key: key);

  @override
  EmployeeUiState createState() => EmployeeUiState();
}

class EmployeeUiState extends State<EmployeeUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();

  EmployeeUiState();

  EmployeeBloc employeeBloc;
  Employee employee = Employee();

  @override
  void dispose() {
    super.dispose();
    employeeBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    employeeBloc = EmployeeBloc();
  }

  void _submitRegisterForm() {
    final formRegister = _formRegister.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      employeeBloc.create(employee).then((ApiResponse data) {
        if (data.statusResponse == 201) {
          Navigator.pushReplacementNamed(context, 'home');
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.appBarregisterEmployee),
          backgroundColor: Colors.indigo,
          automaticallyImplyLeading: false,
          leading: GestureDetector(
            onTap: () {
              Navigator.pushReplacementNamed(context, Constants.administratorUiRoute);
            },
            child: Icon(
              Icons.arrow_back,
              size: 24,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formRegister,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.drive_file_rename_outline),
                              hintText: 'Name'),
                          onSaved: (String value) {
                            employee.name = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.drive_file_rename_outline),
                              hintText: 'Lastname'),
                          onSaved: (String value) {
                            employee.lastName = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.email_outlined),
                              hintText: 'Email'),
                          onSaved: (String value) {
                            employee.email = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.roofing_outlined),
                              hintText: 'Address'),
                          onSaved: (String value) {
                            employee.address = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.phone_android),
                              hintText: 'Phone'),
                          onSaved: (String value) {
                            employee.phone = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.person),
                              hintText: 'Username'),
                          onSaved: (String value) {
                            employee.username = value;
                          },
                        ),
                        Divider(),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.vpn_key),
                              hintText: 'Password'),
                          onSaved: (String value) {
                            employee.password = value;
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () {
                              _submitRegisterForm();
                            },
                            child: Text('Register'),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
