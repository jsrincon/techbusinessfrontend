import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/employee_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';

import '../../../navegator_techbusiness.dart';

class EmployeeListUi extends StatefulWidget {
  @override
  EmployeeListUiState createState() => EmployeeListUiState();
}

class EmployeeListUiState extends State<EmployeeListUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formProcess = GlobalKey<FormState>();

  EmployeeListUiState();

  List<Employee> listEmployee = [];
  Employee employee = Employee();
  EmployeeBloc employeeBloc;

  @override
  void dispose() {
    super.dispose();
    employeeBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    employeeBloc = EmployeeBloc();
    // processBloc.initializeData();
  }

  void _submitChangeStatus(newStatus, Employee employee) {
    employee.status = newStatus;
    employeeBloc.update(employee).then((ApiResponse data) {
      if (data.statusResponse == 200) {
        employeeBloc.initializeData();
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    employeeBloc.initializeData();
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.labelEmployee),
          backgroundColor: ConstantsColors.primaryColor,
          automaticallyImplyLeading: false,
          leading: GestureDetector(
            onTap: () {
              Navigator.pushReplacementNamed(
                  context, Constants.administratorUiRoute);
            },
            child: Icon(
              Icons.arrow_back,
              size: 24,
            ),
          ),
        ),
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            _buildStream(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: FloatingActionButton(
                      onPressed: () {
                        TechBusinessNavigator.goToEmployeeSeachUi(context);
                      },
                      child: Icon(
                        Icons.search,
                        size: 24,
                      ),
                      backgroundColor: ConstantsColors.secondaryColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: FloatingActionButton(
                      onPressed: () {
                        TechBusinessNavigator.goToEmployeeRegisterUi(context);
                      },
                      child: Icon(
                        Icons.add,
                        size: 24,
                      ),
                      backgroundColor: ConstantsColors.secondaryColor,
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Widget _buildStream() {
    return StreamBuilder<List<Employee>>(
        stream: employeeBloc.employeeList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Employee> data) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverPadding(
                padding: const EdgeInsets.all(10.0),
                sliver: SliverList(
                    delegate: SliverChildListDelegate(<Widget>[
                  Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      margin: EdgeInsets.all(5),
                      elevation: 5,
                      color: ConstantsColors.veryLightGray,
                      child: Column(children: [
                        ListTile(
                          title: Text(data[index].name),
                          subtitle: Text(data[index].email),
                          trailing: Wrap(
                            children: [
                              // IconButton(
                              //   icon: Icon(Icons.edit),
                              //   onPressed: () {
                              //     showDialog(
                              //       context: context,
                              //       builder: (_) => _buildPopupDialog(
                              //           context, data[index], false),
                              //     );
                              //   },
                              // ),
                              Switch(
                                value: data[index].status,
                                onChanged: (bool newValue) {
                                  _submitChangeStatus(newValue, data[index]);
                                },
                              ),
                            ],
                          ),
                          isThreeLine: true,
                        )
                      ]))
                ])),
              )
            ],
          );
        });
  }

  Widget _buildPopupDialog(
      BuildContext context, Employee employee, bool isNew) {
    var title = isNew
        ? Constants.labelCreates + ' ' + Constants.labelProcess.toLowerCase()
        : Constants.labelUpdates + ' ' + Constants.labelProcess.toLowerCase();
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Form(
            key: _formProcess,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: isNew ? '' : employee.name,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelName),
                  onSaved: (String value) {
                    employee.name = value;
                  },
                ),
                TextFormField(
                  initialValue: isNew ? '' : employee.phone,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  maxLines: 2,
                  maxLength: 150,
                  decoration:
                      InputDecoration(hintText: Constants.labelDescription),
                  onSaved: (String value) {
                    employee.phone = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                Constants.btnCancel,
                style: TextStyle(color: ConstantsColors.primaryColor),
              ),
            ),
            isNew
                ? ElevatedButton(
                    onPressed: () {
                      //_submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnCreates))
                : ElevatedButton(
                    onPressed: () {
                      // _submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnUpdates)),
          ],
        )
      ],
    );
  }
}
