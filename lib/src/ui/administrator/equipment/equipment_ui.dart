// ignore: avoid_web_libraries_in_flutter

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/equiment_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/ui/administrator/equipment/equipment_detail_ui.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';
import 'package:techbusinessfrontend/src/utils/custom_list_item.dart';

class EquipmentUi extends StatefulWidget {
  const EquipmentUi({Key key}) : super(key: key);

  @override
  EquipmentUiState createState() => EquipmentUiState();
}

class EquipmentUiState extends State<EquipmentUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formService = GlobalKey<FormState>();

  EquipmentUiState();

  List<EquipmentUi> lstEquipment = [];
  Equipment equipment = Equipment();
  EquipmentBLoc equipmentBloc;

  @override
  void dispose() {
    super.dispose();
    equipmentBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    equipmentBloc = EquipmentBLoc();
  }

  void _submitRegisterForm() {
    final formRegister = _formService.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      equipmentBloc.create(equipment).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          equipmentBloc.initializeData();
          Navigator.of(context).pop();
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _submitUpdateForm() {
    final formRegister = _formService.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      equipmentBloc.update(equipment).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.ok) {
          equipmentBloc.initializeData();
          Navigator.of(context).pop();
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    equipmentBloc.initializeData();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelEquipment),
        backgroundColor: ConstantsColors.primaryColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacementNamed(
                context, Constants.administratorUiRoute);
          },
          child: Icon(
            Icons.arrow_back,
            size: 24,
          ),
        ),
      ),
      body: _buildStream(),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     showDialog(
      //       context: context,
      //       builder: (_) => _buildPopupDialog(context, null, true),
      //     );
      //   },
      //   child: Icon(
      //     Icons.add,
      //     size: 24,
      //   ),
      //   backgroundColor: ConstantsColors.primaryColor,
      // ),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<List<Equipment>>(
        stream: equipmentBloc.equipmentList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Equipment> data) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverPadding(
                  padding: const EdgeInsets.all(2.0),
                  sliver: SliverList(
                      delegate: SliverChildListDelegate(<Widget>[
                    CustomListItem(
                      title: data[index].model,
                      description: data[index].mark,
                      status: true,
                      // actionEdit: () {
                      //   showDialog(
                      //     context: context,
                      //     builder: (_) =>
                      //         _buildPopupDialog(context, data[index], false),
                      //   );
                      // },
                      actionShow: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                DetailEquipmentUI(equipment: data[index]),
                          ),
                        );
                      },
                    ),
                    Divider()
                  ])))
            ],
          );
        });
  }

  Widget _buildPopupDialog(
      BuildContext context, Equipment equipmentLoad, bool isNew) {
    isNew ? equipment = Equipment() : equipment = equipmentLoad;
    var title = isNew
        ? Constants.labelCreates + ' ' + Constants.labelService.toLowerCase()
        : Constants.labelUpdates + ' ' + Constants.labelService.toLowerCase();
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Form(
            key: _formService,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: isNew ? '' : equipment.mark,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelName),
                  onSaved: (String value) {
                    equipment.mark = value;
                  },
                ),
                TextFormField(
                  initialValue: isNew ? '' : equipment.model,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  maxLines: 2,
                  maxLength: 150,
                  decoration:
                      InputDecoration(hintText: Constants.labelDescription),
                  onSaved: (String value) {
                    equipment.model = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                Constants.btnCancel,
                style: TextStyle(color: ConstantsColors.primaryColor),
              ),
            ),
            isNew
                ? ElevatedButton(
                    onPressed: () {
                      _submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnCreates))
                : ElevatedButton(
                    onPressed: () {
                      _submitUpdateForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnUpdates)),
          ],
        )
      ],
    );
  }
}
