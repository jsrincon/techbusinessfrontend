import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/equiment_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/alert_helper.dart';
import 'package:techbusinessfrontend/src/utils/constants.dart';
import 'package:techbusinessfrontend/src/utils/constantsColor.dart';

class DetailEquipmentUI extends StatefulWidget {
  final Equipment equipment;

  const DetailEquipmentUI({Key key, this.equipment}) : super(key: key);

  @override
  DetailEquipmentUiState createState() => DetailEquipmentUiState();
}

class DetailEquipmentUiState extends State<DetailEquipmentUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Equipment equipment;
  EquipmentBLoc equipmentBLoc;
  List<Equipment> option;
  List<String> dropdownOptionsMenu = Constants.actionsMenu;

  @override
  void dispose() {
    super.dispose();
    equipmentBLoc.dispose();
  }

  @override
  void initState() {
    super.initState();
    equipment = widget.equipment;
    equipmentBLoc = EquipmentBLoc();
  }

  void _submitUpdate() {
    equipmentBLoc.update(equipment).then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        Navigator.of(context).pop();
        showDialogSuccess(context, 'Guardado exitoso');
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Detail equipment'),
        backgroundColor: ConstantsColors.primaryColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacementNamed(context, Constants.equipmentUiRoute);
          },
          child: Icon(
            Icons.arrow_back,
            size: 24,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(child: Text('id: ' + equipment.id.toString())),
          Expanded(child: Text('model: ' +equipment.model)),
          Expanded(child: Text('mark: ' +equipment.mark)),
          Expanded(child: Text('hardDisk: ' +equipment.hardDisk)),
          Expanded(child: Text('type: ' +equipment.type)),
          Expanded(child: Text('ram: ' +equipment.ram)),
          Expanded(child: Text('processor: ' +equipment.processor)),
          Expanded(child: Text('board: ' +equipment.board)),
          Expanded(child: Text('status: ' +equipment.status))
        ],
      ),
    );
  }
}
