import 'package:flutter/cupertino.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

import 'utils/constants.dart';

class TechBusinessNavigator {
  static void goToHome(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Constants.homeRoute, (Route<dynamic> route) => false);
  }

  static void goToAdministratorUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.administratorUiRoute);
  }

  static void goToAuthUi(BuildContext context) {
    final token = TokenHelper();
    token.logout();
    Navigator.pushNamed(context, Constants.authUiRoute);
  }

  static void goToEmployeeRegisterUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeUiRoute);
  }

  static void goToProcessUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.configUiRoute);
  }

  static void goToEmployeeSeachUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeSeachUiRoute);
  }

  static void goToEmployeeListUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeListUiRoute);
  }
}
